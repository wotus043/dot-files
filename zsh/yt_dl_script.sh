#!/bin/bash

if [[ "$1" = "ck" ]]; then
  youtube-dl -F $2
  exit
fi

# check available formats
if [[ -n $1 ]]; then
  ytdl_ck=$(youtube-dl -F $1)
  tenSix=$(echo "$ytdl_ck" | cut -c 1-3 | grep 299)
  tenThree=$(echo "$ytdl_ck" | cut -c 1-3 | grep 137)
  sevenSix=$(echo "$ytdl_ck" | cut -c 1-3 | grep 298)
  sevenThree=$(echo "$ytdl_ck" | cut -c 1-3 | grep 136)

# debug printing
  echo $tenThree
  echo $tenSix
  echo $sevenThree
  echo $sevenSix
# choose the corresponding way to download starting from the best quality
# more options to be added later
  if [[ "$tenSix" -eq 299 ]]; then
    youtube-dl -f 299+140 $1
    exit

  elif [[ "$tenThree" -eq 137 ]]; then
    youtube-dl -f 137+140 $1
    exit

  elif [[ "$sevenSix" -eq 298 ]]; then
    youtube-dl -f 298+140 $1
    exit

  elif [[ "$sevenThree" -eq 136 ]]; then
    youtube-dl -f 136+140 $1
    exit

  else
    youtube-dl $1
  fi

fi
