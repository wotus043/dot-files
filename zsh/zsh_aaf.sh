# Sudo
function add_sudo() {
		BUFFER="sudo "$BUFFER
		zle end-of-line
	}
	zle -N add_sudo
bindkey "^s" add_sudo

alias smount='sh ~/dot-files/mount_smb.sh'
alias neofetch='neofetch --colors 1 2 3 4 5 6 --battery_display bar --ascii_colors 1 2 3 4 5 6'
alias ffcap='ffmpeg -video_size 1366x768 -framerate 30 -f x11grab -i :0.0 -crf 15'
