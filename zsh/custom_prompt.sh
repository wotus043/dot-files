autoload -U colors && colors

setopt PROMPT_SUBST

set_prompt() {

PS1="%F{204}%n %B%F{146}%~%f%b%F{059}
» "
}

precmd_functions+=set_prompt

preexec () {
   (( ${#_elapsed[@]} > 1000 )) && _elapsed=(${_elapsed[@]: -1000})
   _start=$SECONDS
}

precmd () {
   (( _start >= 0 )) && _elapsed+=($(( SECONDS-_start )))
   _start=-1
}
