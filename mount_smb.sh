#!/bin/bash
# A simple script to mount samba shares on my local network

if [[ $# -eq 0 ]] ; then
    echo 'Please provide the share name'
    exit 1
fi

# List samba shares
if [[ "$1" == "-l" ]] ; then
    smbclient -L 192.168.0.2 -U Baran
    exit 0
fi

# Unmount safely with cd just in case
if [[ "$1" == "-u" ]] ; then
    cd ~/
    sudo umount /home/usagi/media
    exit 0
fi

sudo mount -t cifs //192.168.0.2/$1 /home/usagi/media -o iocharset=utf8,uid=usagi,gid=usagi,credentials=/etc/samba/credentials/share
