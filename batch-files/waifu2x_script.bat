@echo off
cls

echo waifu2x script
echo ------------------
cd /D D:\Program Files\waifu2x-converter-cpp-new

if not exist "waifu2x-converter-cpp.exe" (
	echo waifu2x      : [ERROR] - waifu2x-converter-cpp.exe not found.
) else (
	echo waifu2x      : [OK] - waifu2x-converter-cpp.exe found.
)

echo/
echo 1. Single file mode
echo 2. Batch mode
echo 3. Exit
echo 4. Noise reduction
echo/
set /p choice=Please choose:

if %choice%==1 goto single
if %choice%==2 goto batch
if %choice%==3 goto end
if %choice%==4 goto noise
goto end

:single
cls
echo Taking file from D:\temp\upscale
::cd /d D:\temp
echo/

set /p filename=Input the filename with extension: 

if not exist "D:\temp\upscale\%filename%" (
		echo/
		echo/
		echo Wrong filename, file does not exist
		pause
		goto single
	) else (
		echo/
		echo file        : [OK] - %filename% found.
		echo/
		echo/
		)

set /p scale=Set upscale scale: 


echo/
echo/
echo/
waifu2x-converter-cpp --scale_ratio %scale% -m noise_scale -i "D:\temp\upscale\%filename%" -o "D:\temp\upscale\%filename%.upscaled_%scale%x.png"

goto end

:batch
cls
echo Taking files from D:\temp\upscale
echo Files should be named 1.ext, 2.ext, etc.

echo/

set /p amount=The amount of pictures: 
set /p scale=Set upscale scale: 
echo/
echo/
echo/

for /l %%x in (1, 1, %amount%) do (
   echo %%x >> D:\temp\upscale\log.txt
   waifu2x-converter-cpp --scale_ratio %scale% -m noise_scale -i "D:\temp\upscale\%%x.jpg" -o "D:\temp\upscale\%%x.upscaled_%scale%x.png" || waifu2x-converter-cpp --scale_ratio %scale% -m noise_scale -i "D:\temp\upscale\%%x.png" -o "D:\temp\upscale\%%x.upscaled_%scale%x.png"

)

cd D:\temp\upscale


for /l %%x in (1, 1, %amount%) do (
	del /q %%x.????
)

goto end

:noise
cls
set /p filename=Input the filename with extension: 

echo/
echo/
echo/


waifu2x-converter-cpp --noise_level 2 -m noise -i "D:\temp\upscale\%filename%" -o "D:\temp\upscale\%filename%.noise_reduction.png"

goto end

:end
pause
