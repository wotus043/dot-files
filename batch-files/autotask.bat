@echo off
set tempDir=D:\temp
set desktop=C:\Users\Baran\Desktop


:: encrypt r.txt for backup
gpg --output "%tempDir%\r.gpg" --encrypt -r wbaran@airmail.cc -r wpbaran11@gmail.com "%desktop%\r.txt"

rclone copy "%tempDir%\r.gpg" dropboxy:BACKUPS || echo %date% %time%: r.txt backup wasn't successful >> %desktop%\backup.txt
rclone copy "%tempDir%\r.gpg" gdrive:BACKUPS || echo %date% %time%: r.txt backup wasn't successful >> %desktop%\backup.txt
del /Q "%tempDir%\r.gpg"
:: keyring backup

set keyringPath="C:\Users\Baran\AppData\Roaming\gnupg"

:: check and remove old backup if needed
if exist "%keyringPath%\keyringbackup.zip" (
  del /Q "%keyringPath%\keyringbackup.zip"
  )


7z.exe a "%keyringPath%\keyringbackup.zip" "%keyringPath%\pubring.gpg" "%keyringPath%\pubring.kbx"

rclone copy "%keyringPath%\keyringbackup.zip" dropboxy:BACKUPS || echo %date% %time%: Keyring backup wasn't successful >> %desktop%\backup.txt
rclone copy "%keyringPath%\keyringbackup.zip" gdrive:BACKUPS || echo %date% %time%: Keyring backup wasn't successful >> %desktop%\backup.txt


:: remove the just uploaded backup

del /Q "%keyringPath%\keyringbackup.zip"

:: encrypt db for backup
::gpg --output "%tempDir%\Important.kdbx.gpg" --encrypt -r wbaran@airmail.cc -r wpbaran11@gmail.com "C:\Users\Baran\Documents\Important.kdbx"

:: upload
rclone copy "C:\Users\Baran\Documents\Important.kdbx" dropboxy:BACKUPS || echo %date% %time%: Important.kdbx.gpg backup wasn't successful >> %desktop%\backup.txt
rclone copy "C:\Users\Baran\Documents\Important.kdbx" gdrive:BACKUPS || echo %date% %time%: Important.kdbx.gpg backup wasn't successful >> %desktop%\backup.txt

:: cleanup
::del /Q "%tempDir%\Important.kdbx.gpg"

::gpg --output "%tempDir%\MainDB.kdbx.gpg" --encrypt -r wbaran@airmail.cc -r wpbaran11@gmail.com "C:\Users\Baran\Documents\MainDB.kdbx"


rclone copy "C:\Users\Baran\Documents\MainDB.kdbx" dropboxy:BACKUPS || echo %date% %time%: MainDB.kdbx backup wasn't successful >> %desktop%\backup.txt
rclone copy "C:\Users\Baran\Documents\MainDB.kdbx" gdrive:BACKUPS || echo %date% %time%: MainDB.kdbx backup wasn't successful >> %desktop%\backup.txt

::del /Q "%tempDir%\MainDB.kdbx.gpg"
