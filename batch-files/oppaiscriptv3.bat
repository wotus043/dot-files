::
::
::oppai partial automation script version 3
::made by wotus043
::osu.ppy.sh/u/wotsuoo
::
::created for oppai v0.2.0 (propably working with higer versions)
::tested with v0.9.5
::

@echo off
cls



set osuPath=D:\Games\osu!
set SongPath=%osuPath%\Songs
set osutoolsPath=%osuPath%\osu!tools


if not exist "%osuPath%\osu!.exe" (
	echo osu!      : [ERROR] - osu!.exe not found.
) else (
	echo osu!      : [OK] - %osuPath%\osu!.exe
)



if not exist "%osutoolsPath%\oppai.exe" (
	echo OPPAI     : [ERROR] - oppai.exe  not found.
) else (
	echo OPPAI     : [OK] - %osutoolsPath%\oppai.exe
)
echo/

cd /d %osutoolsPath%

if exist "temp" (
	rmdir /q /s temp
) else (
	rem
)

mkdir temp
goto MapsetNameIN
:restart
del /q /s temp\*
set songnumber=
set mapsetname=
set diffname=
set bmfilename=
cls
goto MapsetNameIN
:restartsame
del /q /s temp\*
set diffname=
set bmfilename=
set acc=
set mods=
cd %SongPath%
cd "%songnumber%*%mapsetname%*"
cls
goto DiffNameIN
:restartdiff
cls
set acc=
set mods=
goto OPPAI
:SongNumberIN
::cls
echo.
echo.
echo Provide the osu mapset number of song

set /p songnumber=
if [%songnumber%] == [] goto MapsetNameIN
goto CustomLocate
:: locate map using mapset !!number!! and diff name


:MapsetNameIN
::cls
echo Provide the mapset name
echo.

set /p mapsetname=


cd %SongPath%
echo Folders that meet this query

::search through all the possible folder names and print them

dir /ad /b "%songnumber%*%mapsetname%*" || goto WrongMapName

echo/
echo/
echo If more than one is found script will use the first on this list.
echo/
echo You can also provide number of mapset here
set /p songnumber=
cd %SongPath%
cd "%songnumber%*%mapsetname%*"
goto DiffNameIN
:CustomLocate

cd %SongPath%
cd "%songnumber%*%mapsetname%*"

goto DiffNameINAlt
:DiffNameIN
cls
echo Currently working on directory %cd%
echo/
echo/
echo Difficulties list
dir /b *.osu
echo/
echo Provide the name of the difficulty

set /p diffname=
copy "*[*%diffname%*].osu" "%osutoolsPath%\temp"
goto OPPAI
:DiffNameINAlt
cls
echo Currently working on directory %cd%
echo/
echo/
echo Difficulties list
dir /b *[*].osu
echo/
echo Provide the name of the difficulty

set /p diffname=
copy "*[*%diffname%*].osu" "%osutoolsPath%\temp"
goto OPPAI
:OPPAI
:: copy .osu file for easier handling in oppai
::
::old copy phase
::
::copy "*[*%diffname%*].osu" "%osutoolsPath%\temp"

cd %osutoolsPath%\temp

:: load the filename into a variable for easier handling in oppai

dir /b /od > beatmap.txt

set /p bmfilename=<beatmap.txt

cd ..
echo Acc?
set /p acc=

echo Mods?
set /p mods=

if [%mods%] == [] goto CustomOPPAI2

goto OPPAI2

::not used as of version 2
cls
echo ---------------------------------------------
echo 100%
echo ---------------------------------------------

oppai "temp\%bmfilename%" +%mods%
echo ---------------------------------------------
echo 99%
echo ---------------------------------------------

oppai "temp\%bmfilename%" "99%%" +%mods%

echo ---------------------------------------------
echo 95%%
echo ---------------------------------------------
pause

oppai "temp\%bmfilename%" "95%%" +%mods%
goto end
:CustomOPPAI
::not used as of version 2
cls
echo ---------------------------------------------
echo 100%
echo ---------------------------------------------

oppai "temp\%bmfilename%"
echo ---------------------------------------------
echo 99%
echo ---------------------------------------------

oppai "temp\%bmfilename%" "99%%"

echo ---------------------------------------------
echo 95%%
echo ---------------------------------------------
pause

oppai "temp\%bmfilename%" "95%%"
goto end
:OPPAI2
cls
if [%acc%] == [] goto OPPAI3
oppai "temp\%bmfilename%" "%acc%%%" +%mods%

goto end
:CustomOPPAI2
cls
if [%acc%] == [] goto CustomOPPAI3

oppai "temp\%bmfilename%" "%acc%%%"

goto end
:OPPAI3
cls
oppai "temp\%bmfilename%" +%mods%

goto end
:CustomOPPAI3
cls
oppai "temp\%bmfilename%"

goto end
:WrongMapName
echo.
echo.
echo No map under this name found

goto end

:end
pause >nul


cls
echo.
echo ACTIONS: (R)e-run   (S)ame beatmap   Same (d)ifficulty   (E)xit
echo.

set /p choiceshitfuck=Choose an action:

if %choiceshitfuck%==r cls & goto restart
if %choiceshitfuck%==e goto finalend
if %choiceshitfuck%==s goto restartsame
if %choiceshitfuck%==d goto restartdiff
:finalend

rmdir /q /s temp
