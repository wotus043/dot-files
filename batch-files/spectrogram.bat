:: This batch file takes in a sound file as an argument and in return opens up a irfanview with the image of the aforementioned sound file's spectrogram

@echo off

ffmpeg -i %1 -lavfi showspectrumpic=s=1024x1024 "C:\temp\spectrogram.png"

"C:\Program Files\IrfanView\i_view64.exe" "C:\temp\spectrogram.png"

del /Q "C:\temp\spectrogram.png"
