#!/bin/bash

scrot /tmp/i3lock.png
convert /tmp/i3lock.png -blur 5x5 /tmp/i3blur.png

i3lock -e -i /tmp/i3blur.png
rm /tmp/i3blur.png
rm /tmp/i3lock.png
