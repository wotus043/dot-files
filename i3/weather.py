#!/usr/bin/env python3

import json
import datetime
import socket
from urllib.request import urlopen
from socket import timeout
import requests
from sys import exit

def getData(source):
    try:
        data = json.load(urlopen(source))
        return data
    except Exception:
        exit(1)

def getLocation():
    try:
        data = requests.get("https://geolocation-db.com/jsonp/")
    except Exception:
        exit(1)

    data = data.content.decode()
    data = data.split("(")[1].strip(")")
    data = json.loads(data)
    lat = str(data["latitude"])
    lgt = str(data["longitude"])
    return lat, lgt

def isNight():
    time = int(datetime.datetime.now().strftime("%H%M"))
    if time > 2000 or time < 600:
        return True
    else:
        return False

def windDirec(wind_direc):
    # convert degrees to NSWE
    if wind_direc > 337 and wind_direc <= 360:
        wind_direc_code="N"
        return ''

    elif wind_direc >= 0 and wind_direc <= 23:
        wind_direc_code="N"
        return ''

    elif wind_direc > 23 and wind_direc <= 68:
        wind_direc_code="NE"
        return ''

    elif wind_direc > 68 and wind_direc <= 113:
        wind_direc_code="E"
        return ''

    elif wind_direc > 113 and wind_direc <= 158:
        wind_direc_code="SE"
        return ''

    elif wind_direc > 158 and wind_direc <= 203:
        wind_direc_code="S"
        return ''

    elif wind_direc > 203 and wind_direc <= 248:
        wind_direc_code="SW"
        return ''

    elif wind_direc > 248 and wind_direc <= 293:
        wind_direc_code="W"
        return ''

    elif wind_direc > 293 and wind_direc <= 337:
        wind_direc_code="NW"
        return ''

def weatherCond(desc):
    if isNight():
        icon_thunderstorm='  '
        icon_drizzle='  '
        icon_rain='  '
        icon_snow='  '
        icon_fog='  '
        icon_clear='  '
    else:
        icon_thunderstorm='  '
        icon_drizzle='  '
        icon_rain='  '
        icon_snow='  '
        icon_fog='  '
        icon_clear='  '

    icon_cloudy='摒 '

    # determine the weather

    if desc == 'Rain':
        return icon_rain

    elif desc == 'Drizzle':
        return icon_drizzle

    elif desc == 'Thunderstorm':
        return icon_thunderstorm

    elif desc == 'Snow':
        return icon_snow

    elif desc == 'Mist' or desc == 'Fog':
        return icon_fog

    elif desc == 'Clear':
        return icon_clear

    elif desc == 'Clouds':
        return icon_cloudy

def tempColor(temp):

    # set color based on temperature (purely polybar function)
    if temp >= 30:
        return "%{F#be5e5e}"
    elif temp < 30 and temp >= 25:
        return "%{F#dab33e}"
    elif temp < 25 and temp >= 15:
        return "%{F#69a744}"
    elif temp < 15 and temp >= 0:
        return "%{F#61c2a2}"
    elif temp < 0:
        return "%{F#5493c1}"

def main():
    apikey = "a66bd66681cffd62a1ab6b9e9f44c414"
    cityid = "7532925"

    location = list(getLocation())

    #weather_source = "http://api.openweathermap.org/data/2.5/forecast?id="+cityid+"&APPID="+apikey
    weather_source = "http://api.openweathermap.org/data/2.5/forecast?lat="+location[0]+"&lon="+location[1]+"&APPID="+apikey

    weather_data = getData(weather_source)

    temp = int(float(weather_data["list"][0]["main"]["temp"]) - 273.15)
    desc = weather_data["list"][0]["weather"][0]["main"]
#    wind_speed = int(weather_data["list"][0]["wind"]["speed"])
#    wind_direc = int(weather_data["list"][0]["wind"]["deg"])
    pressure = int(weather_data["list"][0]["main"]["pressure"])


#    icon_wind=windDirec(wind_direc)
    weather_icon=weatherCond(desc)
    temp_color=tempColor(temp)
    print(temp_color+weather_icon+str(temp)+"°")
#    print(str(temp)+str(wind_speed))
#    print(temp_color)
#    print(weather_icon)
#    print(icon_wind)
#    print(wind_direc)
main()
