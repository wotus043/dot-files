#!/bin/bash
#
# This script toggles the screen backlight storing the old brightness value in /tmp so it can be reused later in toggling the backlight on.


bl_state=$(xbacklight -get)
tmp_bl="/tmp/backlight_state"

if [[ ${bl_state%.*} -eq 0 ]]; then
    if [ -f "$tmp_bl" ]; then
        set_value=$(cat "$tmp_bl")
        xbacklight -set $set_value
        rm "$tmp_bl"
    else
        xbacklight -set 100
    fi
else
    echo "$bl_state" > "$tmp_bl"
    xbacklight -set 0
fi

